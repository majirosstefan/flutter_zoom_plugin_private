package com.decodedhealth.flutter_zoom_plugin;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.platform.PlatformView;
import us.zoom.sdk.InMeetingService;
import us.zoom.sdk.InstantMeetingOptions;
import us.zoom.sdk.JoinMeetingOptions;
import us.zoom.sdk.JoinMeetingParams;
import us.zoom.sdk.StartMeetingParamsWithoutLogin;
import us.zoom.sdk.StartMeetingOptions;
import us.zoom.sdk.MeetingService;
import us.zoom.sdk.MeetingStatus;
import us.zoom.sdk.ZoomError;
import us.zoom.sdk.ZoomSDK;
import us.zoom.sdk.ZoomSDKAuthenticationListener;
import us.zoom.sdk.ZoomSDKInitParams;
import us.zoom.sdk.ZoomSDKInitializeListener;

public class ZoomView  implements PlatformView,
        MethodChannel.MethodCallHandler,
        ZoomSDKAuthenticationListener {
    private final TextView textView;
    private final MethodChannel methodChannel;
    private final Context context;
    private final EventChannel meetingStatusChannel;
    private MethodChannel.Result loginResult;

    ZoomView(Context context, BinaryMessenger messenger, int id) {
        textView = new TextView(context);
        this.context = context;

        methodChannel = new MethodChannel(messenger, "com.decodedhealth/flutter_zoom_plugin");
        methodChannel.setMethodCallHandler(this);

        meetingStatusChannel = new EventChannel(messenger, "com.decodedhealth/zoom_event_stream");
    }

    @Override
    public View getView() {
        return textView;
    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        switch (methodCall.method) {
            case "init":
                init(methodCall, result);
                break;
            case "join":
                joinMeeting(methodCall, result);
                break;
            case "start":
                startMeeting(methodCall, result);
                break;
            case "meeting_status":
                meetingStatus(result);
                break;
            case "login":
                login(methodCall, result);
                break;
            case "meeting_details":
                meetingDetails(methodCall, result);
                break;           
            default:
                result.notImplemented();
        }

    }

    private void meetingDetails(final MethodCall methodCall, final MethodChannel.Result result) {
        InMeetingService inMeetingService = ZoomSDK.getInstance().getInMeetingService();
        String meetingNumber = inMeetingService.getCurrentMeetingID();
        String meetingPassword = inMeetingService.getMeetingPassword();

        if(meetingNumber != null && meetingPassword != null ){
            result.success(new String[]{meetingNumber, meetingPassword});
        }

        result.success(new String[]{"",""});
    }
    
    private void login(final MethodCall methodCall, final MethodChannel.Result result) {
        //Todo

        Map<String, String> options = methodCall.arguments();
        String password = options.get("password");
        String email = options.get("email");

        if(ZoomSDK.getInstance().isLoggedIn()){
            result.success(true);
            return;
        }

        ZoomSDK.getInstance().addAuthenticationListener(this);
            try {
                this.loginResult = result;
                ZoomSDK.getInstance().loginWithZoom(email, password);

            } catch (Exception e) {
                System.out.println("error" + e.getMessage());
                result.error("ERR_UNEXPECTED_EXCEPTIO", e.getMessage(), e.getStackTrace());
            }
    }

    @Override
    public void onZoomSDKLoginResult(long l) {
        // resolve promise
        System.out.println("Real Login result: " + String.valueOf(l));
        if(this.loginResult != null ) {
            this.loginResult.success(true);
            loginResult = null;
        }
    }


    private void init(final MethodCall methodCall, final MethodChannel.Result result) {

        Map<String, String> options = methodCall.arguments();

        ZoomSDK zoomSDK = ZoomSDK.getInstance();

        if(zoomSDK.isInitialized()) {
            List<Integer> response = Arrays.asList(0, 0);
            result.success(response);
            return;
        }

        ZoomSDKInitParams initParams = new ZoomSDKInitParams();
//        initParams.jwtToken = options.get("jwtToken");
        initParams.domain = options.get("domain");
        initParams.enableLog = true;
        initParams.enableGenerateDump = true;
        initParams.appKey = "CWYzkWD278CCFhqDNmkmvkoPmOHhQD5n13Ex";
        initParams.appSecret = "VtXAWQTTt9VP4N3bupGLCJ2IpuaUUAAsXsNu";


        System.out.println("error Code" + initParams.domain);

        zoomSDK.initialize(
                context,
                new ZoomSDKInitializeListener() {

                    @Override
                    public void onZoomAuthIdentityExpired() {

                    }

                    @Override
                    public void onZoomSDKInitializeResult(int errorCode, int internalErrorCode) {
                        List<Integer> response = Arrays.asList(errorCode, internalErrorCode);

                        System.out.println("error Code" + errorCode);
                        System.out.println("internal error Code" + internalErrorCode);
                        if (errorCode != ZoomError.ZOOM_ERROR_SUCCESS) {
                            System.out.println("Failed to initialize Zoom SDK");
                            result.success(response);
                            return;
                        }

                        ZoomSDK zoomSDK = ZoomSDK.getInstance();
                        MeetingService meetingService = zoomSDK.getMeetingService();
                        meetingStatusChannel.setStreamHandler(new StatusStreamHandler(meetingService));
                        result.success(response);
                    }
                },
                initParams);
    }

    private void joinMeeting(MethodCall methodCall, MethodChannel.Result result) {

        Map<String, String> options = methodCall.arguments();

        ZoomSDK zoomSDK = ZoomSDK.getInstance();

        if(!zoomSDK.isInitialized()) {
            System.out.println("Not initialized!!!!!!");
            result.success(false);
            return;
        }

        final MeetingService meetingService = zoomSDK.getMeetingService();

        JoinMeetingOptions opts = new JoinMeetingOptions();
        opts.no_invite = parseBoolean(options, "disableInvite", false);
        opts.no_share = parseBoolean(options, "disableShare", false);
        opts.no_driving_mode = parseBoolean(options, "disableDrive", false);
        opts.no_dial_in_via_phone = parseBoolean(options, "disableDialIn", false);
        opts.no_disconnect_audio = parseBoolean(options, "noDisconnectAudio", false);
        opts.no_audio = parseBoolean(options, "noAudio", false);

        JoinMeetingParams params = new JoinMeetingParams();

        params.displayName = options.get("userId");
        params.meetingNo = options.get("meetingId");
        params.password = options.get("meetingPassword");

        meetingService.joinMeetingWithParams(context, params, opts);

        result.success(true);
    }

    private void startMeeting(MethodCall methodCall, MethodChannel.Result result) {

        Map<String, String> options = methodCall.arguments();

        ZoomSDK zoomSDK = ZoomSDK.getInstance();

        if(!zoomSDK.isInitialized()) {
            System.out.println("Not initialized!!!!!!");
            result.success(false);
            return;
        }

        final MeetingService meetingService = zoomSDK.getMeetingService();
//
//        StartMeetingOptions opts = new StartMeetingOptions();
//        opts.no_invite = parseBoolean(options, "disableInvite", false);
//        opts.no_share = parseBoolean(options, "disableShare", false);
//        opts.no_driving_mode = parseBoolean(options, "disableDrive", false);
//        opts.no_dial_in_via_phone = parseBoolean(options, "disableDialIn", false);
//        opts.no_disconnect_audio = parseBoolean(options, "noDisconnectAudio", false);
//        opts.no_audio = parseBoolean(options, "noAudio", false);
//
//        StartMeetingParamsWithoutLogin params = new StartMeetingParamsWithoutLogin();
//
//		params.userId = options.get("userId");
//        params.displayName = options.get("displayName");
//        params.meetingNo = options.get("meetingId");
//		params.userType = MeetingService.USER_TYPE_API_USER;
//		params.zoomAccessToken = options.get("zoomAccessToken");
//
//        meetingService.startMeetingWithParams(context, params, opts);

        InstantMeetingOptions opts = new InstantMeetingOptions();
        meetingService.startInstantMeeting(context, opts);

        result.success(true);
    }

    private boolean parseBoolean(Map<String, String> options, String property, boolean defaultValue) {
        return options.get(property) == null ? defaultValue : Boolean.parseBoolean(options.get(property));
    }


    private void meetingStatus(MethodChannel.Result result) {

        ZoomSDK zoomSDK = ZoomSDK.getInstance();

        if(!zoomSDK.isInitialized()) {
            System.out.println("Not initialized!!!!!!");
            result.success(Arrays.asList("MEETING_STATUS_UNKNOWN", "SDK not initialized"));
            return;
        }

        MeetingService meetingService = zoomSDK.getMeetingService();

        if(meetingService == null) {
            result.success(Arrays.asList("MEETING_STATUS_UNKNOWN", "No status available"));
            return;
        }

        MeetingStatus status = meetingService.getMeetingStatus();
        result.success(status != null ? Arrays.asList(status.name(), "") :  Arrays.asList("MEETING_STATUS_UNKNOWN", "No status available"));
    }

    @Override
    public void dispose() {}

    @Override
    public void onZoomAuthIdentityExpired() {

    }



    @Override
    public void onZoomSDKLogoutResult(long result) {

    }

    @Override
    public void onZoomIdentityExpired() {

    }
}
