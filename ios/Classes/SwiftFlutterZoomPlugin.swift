import Flutter
import UIKit
import MobileRTC

public class SwiftFlutterZoomPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        
        let factory = ZoomViewFactory(messenger: registrar.messenger())
        registrar.register(factory, withId: "flutter_zoom_plugin")
        
    }
}

public class ZoomViewFactory: NSObject, FlutterPlatformViewFactory {
    
    private weak var messenger: (NSObjectProtocol & FlutterBinaryMessenger)?
    
    init(messenger: (NSObjectProtocol & FlutterBinaryMessenger)?) {
        self.messenger = messenger
        super.init()
    }
    
    public func create(
        withFrame frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?
    ) -> FlutterPlatformView {
        return ZoomView(frame, viewId: viewId, messenger: messenger, args: args)
    }
}

public class AuthenticationDelegate: NSObject, MobileRTCAuthDelegate {
    
    private var result: FlutterResult?
    private var loginResult: FlutterResult?
    
    
    public func onAuth(_ result: FlutterResult?) -> AuthenticationDelegate {
        self.result = result
        return self
    }
    
    public func onLogin(_ result: FlutterResult?) -> AuthenticationDelegate {
        self.loginResult = result
        return self
    }
    
    
    public func onMobileRTCAuthReturn(_ returnValue: MobileRTCAuthError) {

        if returnValue == .success {
            self.result?([0, 0])
        } else {
            self.result?([1, 0])
        }
        
        self.result = nil
    }
    
    public func onMobileRTCLoginReturn(_ returnValue: Int) {
        print("Result \(returnValue) ")
//        NSLog(returnValue.)
        if returnValue == 0 {
            self.loginResult?(true)
        } else {
            NSLog("not logged in")
            self.loginResult?(false)
        }
        
        self.loginResult = nil
    }
    
    
    public func getAuthErrorMessage(_ errorCode: MobileRTCAuthError) -> String {
        
        let message = ""
        return message
    }
}

public class ZoomView: NSObject, FlutterPlatformView, MobileRTCMeetingServiceDelegate, FlutterStreamHandler {
    let frame: CGRect
    let viewId: Int64
    var channel: FlutterMethodChannel
    var authenticationDelegate: AuthenticationDelegate
    
    var statusEventChannel: FlutterEventChannel
    var eventSink: FlutterEventSink?
    
    init(_ frame: CGRect, viewId: Int64, messenger: (NSObjectProtocol & FlutterBinaryMessenger)?, args: Any?) {
        self.frame = frame
        self.viewId = viewId
        self.channel = FlutterMethodChannel(name: "com.decodedhealth/flutter_zoom_plugin", binaryMessenger: messenger!)
        self.authenticationDelegate = AuthenticationDelegate()
        self.statusEventChannel = FlutterEventChannel(name: "com.decodedhealth/zoom_event_stream", binaryMessenger: messenger!)

        super.init()
        
        self.statusEventChannel.setStreamHandler(self)
        self.channel.setMethodCallHandler(self.onMethodCall)
    }
    
    public func view() -> UIView {
        
        let label = UILabel(frame: frame)
        label.text = "Zoom"
        return label
    }
    
    public func onMethodCall(call: FlutterMethodCall, result: @escaping FlutterResult) {
        
        switch call.method {
        case "init":
            self.initZoom(call: call, result: result)
        case "join":
            self.joinMeeting(call: call, result: result)
        case "start":
            self.startMeetingWithLoginUser(call: call, result: result)
        case "login":
            self.login(call: call, result:result)
        case "meeting_status":
            self.meetingStatus(call: call, result: result)
        case "meeting_details":
            self.meetingDetails(call: call, result: result)
        default:
            result(FlutterMethodNotImplemented)
        }
    }
    
    
    public func meetingDetails(call: FlutterMethodCall, result: FlutterResult){
        let password = MobileRTCInviteHelper.sharedInstance().meetingPassword;
        let meetingNumber = MobileRTCInviteHelper.sharedInstance().ongoingMeetingID
        result([password, meetingNumber]);
    }
    
    public func initZoom(call: FlutterMethodCall, result: @escaping FlutterResult)  {
        
        NSLog("WWWWWWWWjzklnkdfljldsjlksjdl")
        let pluginBundle = Bundle(for: type(of: self))
        let pluginBundlePath = pluginBundle.bundlePath
        let arguments = call.arguments as! Dictionary<String, String>
        
        let context = MobileRTCSDKInitContext()
        context.domain = arguments["domain"]!
        context.enableLog = true
        context.bundleResPath = pluginBundlePath
        MobileRTC.shared().initialize(context)
        
        let auth = MobileRTC.shared().getAuthService()
        auth?.delegate = self.authenticationDelegate.onAuth(result)
        auth?.jwtToken = arguments["jwtToken"]
        auth?.sdkAuth()
    }
    
    public func login(call: FlutterMethodCall, result: @escaping FlutterResult)  {
        let arguments = call.arguments as! Dictionary<String, String>
        let auth = MobileRTC.shared().getAuthService()
    
        if(auth?.isLoggedIn() == true){
            result(true);
        }
        _ = self.authenticationDelegate.onLogin(result)
        
        let email = arguments["email"]!
        let password = arguments["password"]!
        auth?.login(withEmail: email, password: password, rememberMe: false);
    }
    
    public func meetingStatus(call: FlutterMethodCall, result: FlutterResult) {
        
        let meetingService = MobileRTC.shared().getMeetingService()
        if meetingService != nil {
            
            let meetingState = meetingService?.getMeetingState()
            result(getStateMessage(meetingState))
        } else {
            result(["MEETING_STATUS_UNKNOWN", ""])
        }
    }
    
    public func joinMeeting(call: FlutterMethodCall, result: FlutterResult) {
        
        let meetingService = MobileRTC.shared().getMeetingService()
        let meetingSettings = MobileRTC.shared().getMeetingSettings()
        
        if meetingService != nil {
            
            let arguments = call.arguments as! Dictionary<String, String?>
            
            meetingSettings?.disableDriveMode(parseBoolean(data: arguments["disableDrive"]!, defaultValue: false))
            meetingSettings?.disableCall(in: parseBoolean(data: arguments["disableDialIn"]!, defaultValue: false))
            meetingSettings?.setAutoConnectInternetAudio(parseBoolean(data: arguments["noDisconnectAudio"]!, defaultValue: false))
            meetingSettings?.setMuteAudioWhenJoinMeeting(parseBoolean(data: arguments["noAudio"]!, defaultValue: false))
            meetingSettings?.meetingShareHidden = parseBoolean(data: arguments["disableShare"]!, defaultValue: false)
            meetingSettings?.meetingInviteHidden = parseBoolean(data: arguments["disableDrive"]!, defaultValue: false)
       
            var params = [
                kMeetingParam_Username: arguments["userId"]!!,
                kMeetingParam_MeetingNumber: arguments["meetingId"]!!
            ]
            
            let hasPassword = arguments["meetingPassword"]! != nil
            if hasPassword {
                params[kMeetingParam_MeetingPassword] = arguments["meetingPassword"]!!
            }
            
            let response = meetingService?.joinMeeting(with: params)
            
            if let response = response {
                print("Got response from join: \(response)")
            }
            result(true)
        } else {
            result(false)
        }
    }

    public func startMeeting(call: FlutterMethodCall, result: FlutterResult) {
        
        let meetingService = MobileRTC.shared().getMeetingService()
        let meetingSettings = MobileRTC.shared().getMeetingSettings()
        
        if meetingService != nil {
            
            let arguments = call.arguments as! Dictionary<String, String?>
            
            meetingSettings?.disableDriveMode(parseBoolean(data: arguments["disableDrive"]!, defaultValue: false))
            meetingSettings?.disableCall(in: parseBoolean(data: arguments["disableDialIn"]!, defaultValue: false))
            meetingSettings?.setAutoConnectInternetAudio(parseBoolean(data: arguments["noDisconnectAudio"]!, defaultValue: false))
            meetingSettings?.setMuteAudioWhenJoinMeeting(parseBoolean(data: arguments["noAudio"]!, defaultValue: false))
            meetingSettings?.meetingShareHidden = parseBoolean(data: arguments["disableShare"]!, defaultValue: false)
            meetingSettings?.meetingInviteHidden = parseBoolean(data: arguments["disableDrive"]!, defaultValue: false)

            let user: MobileRTCMeetingStartParam4WithoutLoginUser = MobileRTCMeetingStartParam4WithoutLoginUser.init()
            
            user.userType = .apiUser
            user.meetingNumber = arguments["meetingId"]!!
            user.userName = arguments["displayName"]!!
           // user.userToken = arguments["zoomToken"]!!
            user.userID = arguments["userId"]!!
            user.zak = arguments["zoomAccessToken"]!!

            let param: MobileRTCMeetingStartParam = user
            
            let response = meetingService?.startMeeting(with: param)
            
            if let response = response {
                print("Got response from start: \(response)")
            }
            result(true)
        } else {
            result(false)
        }
    }
    
    public func startMeetingWithLoginUser(call: FlutterMethodCall, result: FlutterResult) {
        
        let meetingService = MobileRTC.shared().getMeetingService()
        
        if meetingService != nil {
            
            let arguments = call.arguments as! Dictionary<String, String?>
            let user: MobileRTCMeetingStartParam4LoginlUser = MobileRTCMeetingStartParam4LoginlUser.init()
            
            // arguments["meetingId"]!
            user.meetingNumber = "" // to always have instant meeting
            user.isAppShare = false

            let param: MobileRTCMeetingStartParam = user
            let response = meetingService?.startMeeting(with: param)
            
            if let response = response {
                print("Got response from start: \(response)")
            }
            result(true)
        } else {
            result(false)
        }
    }
    
    private func parseBoolean(data: String?, defaultValue: Bool) -> Bool {
        var result: Bool
        
        if let unwrappeData = data {
            result = NSString(string: unwrappeData).boolValue
        } else {
            result = defaultValue
        }
        return result
    }
    
    
    
    
    public func onMeetingError(_ error: MobileRTCMeetError, message: String?) {
        
    }
    
    public func getMeetErrorMessage(_ errorCode: MobileRTCMeetError) -> String {
        
        let message = ""
        // switch (errorCode) {
        //     case MobileRTCAuthError_Success:
        //         message = "Authentication success."
        //         break
        //     case MobileRTCAuthError_KeyOrSecretEmpty:
        //         message = "SDK key or secret is empty."
        //         break
        //     case MobileRTCAuthError_KeyOrSecretWrong:
        //         message = "SDK key or secret is wrong."
        //         break
        //     case MobileRTCAuthError_AccountNotSupport:
        //         message = "Your account does not support SDK."
        //         break
        //     case MobileRTCAuthError_AccountNotEnableSDK:
        //         message = "Your account does not support SDK."
        //         break
        //     case MobileRTCAuthError_Unknown:
        //         message = "Unknown error.Please try again."
        //         break
        //     default:
        //         message = "Unknown error.Please try again."
        //         break
        // }
        return message
    }
    
    public func onMeetingStateChange(_ state: MobileRTCMeetingState) {
        
        guard let eventSink = eventSink else {
            return
        }
        
        eventSink(getStateMessage(state))
    }
    
    public func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        self.eventSink = events
        
        let meetingService = MobileRTC.shared().getMeetingService()
        if meetingService == nil {
            return FlutterError(code: "Zoom SDK error", message: "ZoomSDK is not initialized", details: nil)
        }
        meetingService?.delegate = self
        
        return nil
    }
     
    public func onCancel(withArguments arguments: Any?) -> FlutterError? {
        eventSink = nil
        return nil
    }
    
    private func getStateMessage(_ state: MobileRTCMeetingState?) -> [String] {
        
        var message: [String]
        switch state {
        case  .idle:
            message = ["MEETING_STATUS_IDLE", "No meeting is running"]
            break
        case .connecting:
            message = ["MEETING_STATUS_CONNECTING", "Connect to the meeting server"]
            break
        case .inMeeting:
            message = ["MEETING_STATUS_INMEETING", "Meeting is ready and in process"]
            break
        case .webinarPromote:
            message = ["MEETING_STATUS_WEBINAR_PROMOTE", "Upgrade the attendees to panelist in webinar"]
            break
        case .webinarDePromote:
            message = ["MEETING_STATUS_WEBINAR_DEPROMOTE", "Demote the attendees from the panelist"]
            break
        case .disconnecting:
            message = ["MEETING_STATUS_DISCONNECTING", "Disconnect the meeting server, leave meeting status"]
            break;
        case .ended:
            message = ["MEETING_STATUS_ENDED", "Meeting ends"]
            break;
        case .failed:
            message = ["MEETING_STATUS_FAILED", "Failed to connect the meeting server"]
            break;
        case .reconnecting:
            message = ["MEETING_STATUS_RECONNECTING", "Reconnecting meeting server status"]
            break;
        case .waitingForHost:
            message = ["MEETING_STATUS_WAITINGFORHOST", "Waiting for the host to start the meeting"]
            break;
        case .inWaitingRoom:
            message = ["MEETING_STATUS_IN_WAITING_ROOM", "Participants who join the meeting before the start are in the waiting room"]
            break;
        default:
            message = ["MEETING_STATUS_UNKNOWN", "\(state?.rawValue ?? 9999)"]
        }
        
        return message
    }
    
}
