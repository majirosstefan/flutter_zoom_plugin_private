import 'dart:async';
import 'dart:io';

import 'package:flutter_zoom_plugin/zoom_view.dart';
import 'package:flutter_zoom_plugin/zoom_options.dart';

import 'package:flutter/material.dart';


var wasInitialized = false;


var wasLoggedIn = false;

class StartMeetingWidget extends StatelessWidget {
  ZoomOptions zoomOptions;
  ZoomMeetingOptions meetingOptions;

  Timer timer;

  StartMeetingWidget({Key key, meetingId}) : super(key: key) {
    this.zoomOptions = new ZoomOptions(
      domain: "zoom.us",
      jwtToken:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcHBLZXkiOiJDV1l6a1dEMjc4Q0NGaHFETm1rbXZrb1BtT0hoUUQ1bjEzRXgiLCJpYXQiOjE2MTU1MTA3OTksImV4cCI6MTY0NzAxNzk5OSwidG9rZW5FeHAiOjE2NDcwMTc5OTl9.YOe89CfOXyjA9gSO5SmgPfTPZ_VS8iRUUQEUKkwxgQA",
    );
    this.meetingOptions = new ZoomMeetingOptions(     
      meetingId: "",
    );
  }

  bool _isMeetingEnded(String status) {
    if (Platform.isAndroid)
      return status == "MEETING_STATUS_DISCONNECTING" ||
          status == "MEETING_STATUS_FAILED";
    return status == "MEETING_STATUS_ENDED";
  }


  // initZoom must be called only once
  initZoom(controller) async {    
    if(wasInitialized == false){
      wasInitialized = true;
      return controller.initZoom(this.zoomOptions);
    } else {
      print("Skipping");
      return Future.value([0,0]);
    }
  }


  @override
  Widget build(BuildContext context) {
    // Use the Todo to create the UI.
    return Scaffold(
      appBar: AppBar(
        title: Text('Loading meeting '),
      ),
      body: Padding(
          padding: EdgeInsets.all(16.0),
          child: ZoomView(onViewCreated: (controller) {
            print("Created the view");

            this.initZoom(controller).then((results) {
              print("initialised");
              print(results);

              if (results[0] == 0) {
                controller.zoomStatusEvents.listen((status) {
                  print("Meeting Status Stream: " +
                      status[0] +
                      " - " +
                      status[1]);
                  if (_isMeetingEnded(status[0])) {
                    Navigator.pop(context);
                    timer?.cancel();
                  }
                });

                print("listen on event channel");

                controller
                    .login(new ZoomLoginOptions(
                        email: "ssmajiross@gmail.com", password: "12345Barna"))
                    .then((loginResult) {
                  print("Login result " + loginResult.toString());
                  controller
                      .startMeeting(this.meetingOptions)
                      .then((joinMeetingResult) {
                    timer = Timer.periodic(new Duration(seconds: 3), (timer) {
                      controller
                          .meetingStatus(this.meetingOptions.meetingId)
                          .then((status) {
                        print("Meeting Status Polling: " +
                            status[0] +
                            " - " +
                            status[1]);
                      });
                      print("taking meeeting details");
                      controller.meetingDetails().then((details) {
                        print(details.toString());
                      }).catchError((error) {
                        print("error");
                        print(error);
                      });
                    });
                  });
                });
              }
            }).catchError((error) {
              print("Error");
              print(error);
            });
          })),
    );
  }
}
